// Remember to credit: https://bitbucket.org/xerial/sqlite-jdbc
// Licens: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

package DBMS;

import Interface.DBMS;
/**
 *
 * @author Joa
 */
public class SqLite extends DBMS {
    /**
     * @param databasePath e.g.: MyOwnDatabase.db
     */
    public SqLite(String host, String databasePath, String username, String password) {
        super();
        
        host("");
        database(databasePath);
        username("");
        password("");
        
        driver("org.sqlite.JDBC");
        client("jdbc:sqlite:");
        port("");
    }
    
    public SqLite() {
        super();
        driver("org.sqlite.JDBC");
        client("jdbc:sqlite:");
        port("");
    }
    
    public SqLite(String databasePath) {
        host("");
        database(databasePath);
        username("");
        password("");
        
        driver("org.sqlite.JDBC");
        client("jdbc:sqlite:");
        port("");
    }
}
