package Model;

import java.io.File;
import java.io.FileOutputStream;

// Remeber to credit: https://poi.apache.org/
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.*;

import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
/**
 *
 * @author Joa
 */
public class XlsConverter {
    private HSSFWorkbook workbook = null; // workbook object
    private ArrayList<HSSFSheet> sheets = null; // Stack of workbook sheets
    
    public void createWorkBook(String id, String sheetname, String filename, 
            ResultSet rs) {
        workbook = new HSSFWorkbook();
        sheets = new ArrayList<>(); 
        HSSFSheet sheet = workbook.createSheet(sheetname);
        int rownum = 0;
        int cellnum = 0;
        Row row = sheet.createRow(rownum++);
        try {
            ResultSetMetaData md = rs.getMetaData();
            
            for(int i = 1; i <= md.getColumnCount(); i++) { // Set Column Headers
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(md.getColumnName(i));
            }
            while(rs.next()) {                                  // Set Row Data
                row = sheet.createRow(rownum++);
                cellnum = 0;
                for(int j = 1; j <= md.getColumnCount(); j++) {
                    Cell cell = row.createCell(cellnum++);
                    cell.setCellValue(String.valueOf(rs.getObject(j)));
                } 
            }
        } catch (SQLException e) {
            // Make a log
        }
        System.out.println("Job: "+id+", " + rownum);
        sheets.add(sheet);
        
        if(workbook != null) {
            try {
                FileOutputStream output_file = new FileOutputStream(new File(filename)); //create XLS file
                workbook.write(output_file);
                output_file.close();
            } catch (Exception e) {
                // Make a log
            }
        }
    }
}
