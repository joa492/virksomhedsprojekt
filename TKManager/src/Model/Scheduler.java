package Model;

import Interface.ColumnIndex;
import java.util.Timer;
import java.util.TimerTask;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * DbManager job = new DbManager(new DBMS.SqLite("MyJobDB.db"));
 * DbManager main = new DbManager(new DBMS.SqLite("MyMainDB.db"));
 * 
 * String extractJobs = "SELECT ID, column2, sqlJob FROM Job";  
 * 
 * Scheduler sc = new Scheduler(job, main, extractJobs);
 * 
 * sc.start(2, 60); // Starts a TimerTask, which allows the program to continue 
 *                  // It extracts sql jobs every 60 seconds.
 *                  //
 *                  // Each job gets its own thread, executes the query &
 *                  // Saves the result as .xls
 * 
 * sc.stop();       // Cancelled the timed task.
 *                  // It does not free any memeory
 * 
 * @author Joa
 */
public class Scheduler {
    public Scheduler(
            DbManager jobDatabase, 
            DbManager mainDatabase, 
            String getJobs) 
    {
        this.jobDatabase = jobDatabase;
        this.mainDatabase = mainDatabase;
        this.getJobs = getJobs;
    }
    
    public void start(int timeUntilStartUp, int onceEvery) {       
        timer = new Timer();
        timer.schedule(new Job(), timeUntilStartUp, onceEvery*1000);
    }   
    
    public void stop() {
        timer.cancel();
    }
    
    private class Job extends TimerTask {
        @Override
        public void run() {
            System.out.println("Fetching Jobs");
            
            jobDatabase.openConnection();
            
            ResultSet rs = jobDatabase.extract(getJobs);    // Get SQL jobs
            String id, sJob;
            try {
                while(rs.next()) {
                    sJob = rs.getString(ColumnIndex.SQL.getIndex());
                    id = rs.getString(ColumnIndex.ID.getIndex());
                    JobEvent je = new JobEvent(id, sJob, mainDatabase);  // Run Job
                    je.start();
                }    
            } catch (SQLException e) {
                // Make a log
                System.out.println("Scheduler error: "+e);
            }
            
            jobDatabase.closeConnection(); 
        }
    }
    
    private class JobEvent extends Thread implements Runnable {
        private final String sqlJob, id;
        private final DbManager db;
        private final XlsConverter xls = new XlsConverter();
        private final ResultAnalyzer ra = new ResultAnalyzer();
        public JobEvent(String id, String sqljob, DbManager db) {
            this.sqlJob = sqljob;
            this.id = id;
            this.db = db;
        }
        @Override
        public void run() {
            System.out.println("Job: "+id+": Start");
            db.openConnection();
            
            // Execute job against mainDatabase
            ResultSet rs = db.extract(sqlJob);
            
            // Analyze result
            //boolean containsError = ra.analyzeResult(id, rs);
            
            // Export analyzed result
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date dateobj = new Date();
            xls.createWorkBook(id, "tempo", "Results/"+id+"/"+id+"_"+df.format(dateobj)+".xls", rs);
            
            // Find en løsning - så vi ikke skal vente på forbindelsen timerout!
            //db.closeConnection();
            
            // Store JobEvent
            insertJobEvent( 
                    Integer.parseInt(id), 
                    ("Results/"+id+"/"+id+"_"+df.format(dateobj)+".xls"),
                    ""+df.format(dateobj),
                    true
            );
            
            System.out.println("Job: "+id+": Done");
        }
    }
    
    private boolean insertJobEvent( 
            int id, 
            String result_path, 
            String valuer_date, 
            boolean success
    ) {               
        String[] param = {""+id, result_path, valuer_date, ""+(success ? 1 : 0)};
        boolean rt = insertIntoJobDb(param, "JobEvent");
        return rt;
    }
    
    /**
     * insertJob(db, 2, "24", "select name, lastName, phonenumber from Person", "d");
     * 
     * @param id
     * @param start_day
     * @param sql_query
     * @param interval
     * @return 
     */
    public boolean insertJob(
            int id, 
            String start_day, 
            String sql_query, 
            String interval
    ) {
        // Check parameters:
        // id       : should not be in use
        // start_day: char(2):= {1, .., 28}
        // sql_query: text
        // interval : char(1):= {d, m, y}
        // Return error if the format is wrong
        
        // Create directory: id
        // Return error if it already exists
        java.io.File nDir = new java.io.File("Results/"+id);
        
        if(nDir.exists())
            return false; // Dir already exists
        else
            nDir.mkdir();
        
        String[] param = {""+id, start_day, sql_query, interval};
        boolean rt = insertIntoJobDb(param, "Job");
        
        return rt;
    }
    
    /**
     * This method has to be synchroized, due to the fact that
     * a SqLite database i just a regular file, which means, if two
     * threads were to open the file at the same time, any written content
     * would have a chance to be corrupted.
     * 
     * @param param 
     * @param table
     * @return 
     */
    synchronized private boolean insertIntoJobDb(String[] param, String table) {
        while(jobDatabase.isRunning()) {
            // Wait until the jobDatabase object is free to use.
            // 
            // This is only used if Job class is to slow to deploy
            // all the jobEvents.
            // In that case the jobDatabse object could open the connection 
            // twice which would cause an error.
            //
            // Otherwise the synchronized method is only called 
            // once at a time, eventhough multiple threads are running
            // at the same time.
        }
        jobDatabase.openConnection();
        boolean rt = jobDatabase.insert(param, table);
        jobDatabase.closeConnection();
        return rt;
    }
    
    private final String getJobs;
    
    private Timer timer;
    
    private final DbManager jobDatabase;
    private final DbManager mainDatabase;
}
