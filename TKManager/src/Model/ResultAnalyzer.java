package Model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
/**
 *
 * @author Joa
 */
public class ResultAnalyzer {
   
    /**
     * Go throuh a java.sql.ResultSet,
     * Return false if any cell contains zero(0), which should mark,
     * that the ResultSet contains an error
     * 
     * @param id identifier of the resultset
     * @param rs resultset which cotains a format for every row like: 
     *           ID, Kri 1, .., Kri n
     *           where every Kri contains 0 or 1
     * @return whether or not the resultset contains any errors
     */
    public boolean analyzeResult(String id, ResultSet rs) {
        
        boolean error = false;
        // Go through resultset
        try {
            ResultSetMetaData md = rs.getMetaData();
            
            for(int i = 1; i <= md.getColumnCount(); i++) {
            }
            outer:
            while(rs.next()) {                                 
                for(int j = 1; j <= md.getColumnCount(); j++) {
                    error = String.valueOf(rs.getObject(j)).equals("0");
                    break outer;
                } 
            }
        } catch (SQLException e) {
            // Make a log
        }
        
        // Send Mail on Error
        
        return error;
    }
}
