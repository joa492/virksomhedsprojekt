
package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileReader;
import java.util.ArrayList;
/**
 * E.g.:
FileLoader fl = new FileLoader();
ArrayList<DbManager> dbs = fl.reader("config.txt");

dbs.get(0).openConnection();
java.sql.ResultSet rs = dbs.get(0).extract("select * from Job");
try {
while(rs.next())
    System.out.println(rs.getString(1));
}
catch(Exception e) {
    System.out.println(e);
}
dbs.get(0).closeConnection();
 */

/**
 *
 * @author Joa
 */
public class FileLoader {
    /**
     * Method returns the scheduler query, or null if no file has been parsed
     * @return 
     */
    public String query() {
        return this.schedulerquery;
    }
    /**
     * @param filePath Path to file which should be read
     * @return an array of DbManagers - null upon error
     */
    public ArrayList<DbManager> reader(String filePath) {
        ArrayList<DbManager> managers = new ArrayList<>();
        try {
            BufferedReader re = new BufferedReader(new FileReader(filePath));
            
            String line = re.readLine();
            Interface.DBMS dbms = null;
            while(line != null) {
                if(line.startsWith("#") && !line.contains("%")) {
                   managers.add(new DbManager(parseDb(line)));
                }
                else if(line.startsWith("query:")) {
                    this.schedulerquery = line.split(":")[1];
                }
                
                // do something with line
                line = re.readLine();
            }
            re.close();
            return managers;
        }
        catch(FileNotFoundException e) {
            // The specified file could not be found
            System.out.println(e);
        }
        catch(IOException e) {
            // Something went wrong with reader or closing
            System.out.println(e);
        }
        return null;
    }
    
    private Interface.DBMS parseDb(String args) {
        Interface.DBMS dbms;
        
        /**
         * tokens[0] = type
         * tokens[1] = server
         * tokens[2] = username
         * tokens[3] = password
         * tokens[4] = database
         */
        String[] tokens = args.substring(2, args.length()-1).split(":");
        
        try {
            dbms = (Interface.DBMS) createObject("DBMS."+tokens[0]);
            dbms.host(parseArg(tokens[1]));
            dbms.username(parseArg(tokens[2]));
            dbms.password(parseArg(tokens[3]));
            dbms.database(tokens[4]);
            return dbms;
        }
        catch(Exception e)  {
            System.out.println("Database type not supported: "+tokens[0]);
            return null;
        }
        
        
        /*
        if(tokens[0].equals("sqlite"))
            dbms = new DBMS.SqLite(tokens[4]);
        else if(tokens[0].equals("mssql"))
            dbms = new DBMS.MsSql(tokens[1], tokens[4], tokens[2], tokens[3]);
        else if(tokens[0].equals("mysql"))
            dbms = new DBMS.MySql(tokens[1], tokens[4], tokens[2], tokens[3]);
        return (Interface.DBMS) c.newInstance();
                */
    }
    
    private String parseArg(String arg) {
        return arg.equals("#") ? "" : arg;
    }
    
    private Object createObject(String className) {
      Object object = null;
      try {
          Class classDefinition = Class.forName(className);
          object = classDefinition.newInstance();
      } catch (InstantiationException e) {
          System.out.println(e);
      } catch (IllegalAccessException e) {
          System.out.println(e);
      } catch (ClassNotFoundException e) {
          System.out.println(e);
      }
      return object;
   }
    
   private String schedulerquery;

}
