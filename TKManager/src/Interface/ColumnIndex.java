package Interface;
/**
 *
 * @author Joa
 */
public enum ColumnIndex {
    // Job Table
    ID(1),
    START_DAY(2),
    SQL(3),
    INTERVAL(4),
    // JobEvent Table
    // ID(1)
    RESULTPATH(2),
    VALUERDATE(3),
    SUCCESS(4);
    
    private final int index;
    private ColumnIndex(int i) { this.index = i; };
    public int getIndex() { return this.index; };
    
}
