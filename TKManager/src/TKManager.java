
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import Model.*;
import java.util.ArrayList;

/**
 *
 * @author Joa
 */
public class TKManager {    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Starting program");
        
        FileLoader fl = new FileLoader();
        
        // Get DbManagers
        // db.get(0) : JobDatabase
        // db.get(1) : MainDatabase
        ArrayList<DbManager> dbs = fl.reader("config.txt");
        
        if(dbs == null) {
            System.out.println("Error in config.txt file");
            System.exit(0);
        }

        System.out.println("MainDatabase:");
        System.out.println(
                dbs.get(1).dbms().host() + " \n" +
                dbs.get(1).dbms().database()+ " \n" +
                dbs.get(1).dbms().username() + " \n" +
                dbs.get(1).dbms().password() + " \n"
        );
        
        // Get Sql for scheduled jobs
        String schedulerquery = fl.query();
        
        // Structure
        // JobDatase Manager
        // MainDatabase Manager
        // SQL to retrieve scheduled jobs from jobDatabase
        Scheduler sc = new Scheduler(
                dbs.get(0),
                dbs.get(1),
                schedulerquery
        );
        
        // Listen for new jobs
        try {
            System.out.println("To begin the scheduler type:");
            System.out.println("begin:secondsToStart:secondsToRepeat");
            System.out.println();
            System.out.println("Write: quit & press enter to exit the application");
            
            while(true) {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String nString = br.readLine();
                
                /**
                 * begin secondsToStart secondsToRepeat
                 */
                if(nString.startsWith("begin")) {
                    try {
                        String[] tokens = nString.split(":");
                        int ss = Integer.parseInt(tokens[1]);
                        int sr = Integer.parseInt(tokens[2]);
                        sc.start(ss, sr);
                    }
                    catch(Exception e) {
                        System.out.println("Bad arguments");
                        System.out.println("To begin the scheduler type:");
                        System.out.println("begin:secondsToStart:secondsToRepeat");
                    }
                }
                    
                if(nString.equals("quit"))
                        break;
            }
        } catch (IOException e) {
            // Mage log
        }
        
        // Stop scheduling
        sc.stop();
    }
    
    private void foo() {
        
        // Information
        DBMS.MySql my = new DBMS.MySql(
                "mysql11.unoeuro.com",
                "virtuelbesoegsven_dk0_db",
                "virtuelbeso_dk0",
                "Fireben1125"
        );
        DBMS.SqLite sq = new DBMS.SqLite(
                "JobDatabase.db"
        );
        
        // Operations
        Model.DbManager mainDatabaseManager = new Model.DbManager(
                my
        );
        Model.DbManager jobDatabaseManager = new Model.DbManager(
                sq
        );
        
        // Structure
        // JobDatase Manager
        // MainDatabase Manager
        // SQL to retrieve scheduled jobs from jobDatabase
        Model.Scheduler s = new Model.Scheduler(
                jobDatabaseManager, 
                mainDatabaseManager, 
                "SELECT id, start_day, sql_query from ( " +
                "SELECT j.id, j.interval, j.start_day, j.sql_query, " +
                "MAX(je.valuer_date) as 'last' " +
                "FROM Job j LEFT JOIN JobEvent je on j.id = je.job_id " + 
                "GROUP BY j.id, j.interval, j.start_day, j.sql_query) as ljob " +
                "WHERE last is null and strftime('%d', 'now') >= start_day " +
                "( interval = 'm' and date('now') >= date(substr(last, 0, 9) || start_day, '+1 month') ) or " +
                " ( interval = 'y' and date('now') >= date(substr(last, 0, 9) || start_day, '+1 year') ) or " +
                " ( interval = 'd' and date('now') >= date(last, '+1 day') ) "
        );
        
        // Look for sql's to run every 60 seconds
        s.start(2, 60);
        
        s.stop();
    }
    
}
